package kz.astana.lesson20example;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class ContactsActivity extends AppCompatActivity {

    private ArrayAdapter<String> adapter;
    private EditText nameEditText, phoneEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        ActivityCompat.requestPermissions(
                ContactsActivity.this,
                new String[]{
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.SEND_SMS
                },
                200
        );

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String phoneNumber) {
                super.onCallStateChanged(state, phoneNumber);
                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    Log.d("Hello", "IDLE");
                } else if (state == TelephonyManager.CALL_STATE_RINGING) {
                    Log.d("Hello", "RINGING");
                } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    Log.d("Hello", "OFFHOOK");
                }
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);

        ListView listView = findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(ContactsActivity.this, android.R.layout.simple_list_item_1, new ArrayList<>());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String contact = adapter.getItem(position);
                String[] info = contact.split("\n");
                String[] phone = info[2].split(":");
                String[] ids = info[0].split(":");
                showDialog(ids[1].trim(), phone[1].trim());
            }
        });

        nameEditText = findViewById(R.id.nameEditText);
        phoneEditText = findViewById(R.id.phoneNumberEditText);
    }

    public void loadClicked(View view) {
        ActivityCompat.requestPermissions(
                ContactsActivity.this,
                new String[]{Manifest.permission.READ_CONTACTS},
                100);
    }

    public void insertClicked(View view) {
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, nameEditText.getText().toString());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneEditText.getText().toString());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
        startActivity(intent);
    }

    public void showDialog(String id, String phone) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactsActivity.this);
        builder.setItems(new String[]{
                "Edit",
                "Call",
                "Sms intent",
                "Sms manager"
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        editContact(id);
                        break;
                    case 1:
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phone));
                        startActivity(intent);
                        break;
                    case 2:
                        Intent sms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms: + " + phone));
                        startActivity(sms);
                        break;
                    case 3:
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(phone, null, "I'm manager", null, null);
                        break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Cursor cursor = getContentResolver().query(
                    ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor.moveToFirst()) {
                getContactInfo(cursor);
            }
        }
    }

    private void getContactInfo(Cursor cursor) {
        int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
        int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int hasNumberIndex = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        adapter.clear();
        do {
            String id = cursor.getString(idIndex);
            String name = cursor.getString(nameIndex);
            String hasNumber = cursor.getString(hasNumberIndex);

            if (Integer.parseInt(hasNumber) > 0) {
                Cursor phoneCursor = getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[]{id},
                        null
                );
                if (phoneCursor.moveToFirst()) {
                    getPhoneNumber(phoneCursor, id, name);
                }
            }
        } while (cursor.moveToNext());
    }

    private void getPhoneNumber(Cursor phoneCursor, String id, String name) {
        int phoneIndex = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        do {
            String phoneNumber = phoneCursor.getString(phoneIndex);

            String result = "ID: " + id +
                    "\nName: " + name +
                    "\nPhone: " + phoneNumber;
            adapter.add(result);
            adapter.notifyDataSetChanged();
        } while (phoneCursor.moveToNext());
    }

    private void editContact(String id) {
        Cursor cursor = getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                ContactsContract.Contacts._ID + " = ?",
                new String[]{id},
                null
        );

        if (cursor.moveToFirst()) {
            int lookupKeyIndex = cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
            String lookupKey = cursor.getString(lookupKeyIndex);
            Uri contactUri = ContactsContract.Contacts.getLookupUri(Long.parseLong(id), lookupKey);
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setDataAndType(contactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
            startActivity(intent);
        }
    }

    public void searchClicked(View view) {
        String name = nameEditText.getText().toString();
        if (!TextUtils.isEmpty(name)) {
            Cursor cursor = getContentResolver().query(
                    ContactsContract.Contacts.CONTENT_URI,
                    null,
                    ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?",
                    new String[]{"%" + name + "%"},
                    null
            );
            if (cursor.moveToFirst()) {
                int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                int hasNumberIndex = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                adapter.clear();
                do {
                    String id = cursor.getString(idIndex);
                    String hasNumber = cursor.getString(hasNumberIndex);

                    if (Integer.parseInt(hasNumber) > 0) {
                        Cursor phoneCursor = getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id},
                                null
                        );
                        if (phoneCursor.moveToFirst()) {
                            getPhoneNumber(phoneCursor, id, name);
                        }
                    }
                } while (cursor.moveToNext());
            }
        }
    }
}