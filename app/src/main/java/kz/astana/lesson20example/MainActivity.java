package kz.astana.lesson20example;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.view.View;

import java.util.Calendar;
import java.util.GregorianCalendar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(
                MainActivity.this,
                new String[]{
                        Manifest.permission.READ_CALENDAR,
                        Manifest.permission.WRITE_CALENDAR
                },
                100
        );
    }

    public void contactsClicked(View view) {
        startActivity(new Intent(MainActivity.this, ContactsActivity.class));
    }

    public void calendarClicked(View view) {
        Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
        builder.appendPath("time");
        ContentUris.appendId(builder, System.currentTimeMillis());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(builder.build());
        startActivity(intent);
    }

    public void createEvent(View view) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra(CalendarContract.Events.TITLE, "My event");
        intent.putExtra(CalendarContract.Events.DESCRIPTION, "Some new event");
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "Own home");

        GregorianCalendar calendar = new GregorianCalendar(2020, 11, 7);
        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false);
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calendar.getTimeInMillis());
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calendar.getTimeInMillis());
        startActivity(intent);
    }

    public void createAlarm(View view) {
        Intent intent = new Intent();
        intent.setAction(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "Wake up");
        intent.putExtra(AlarmClock.EXTRA_HOUR, 16);
        intent.putExtra(AlarmClock.EXTRA_MINUTES, 56);
        startActivity(intent);
    }

    public void createEventWithReminder(View view) {
        ContentResolver contentResolver = getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CalendarContract.Events.TITLE, "Android");
        contentValues.put(CalendarContract.Events.DESCRIPTION, "New android party");
        Calendar calendar = Calendar.getInstance();
        calendar.set(2020, 11, 5, 14, 0);
        contentValues.put(CalendarContract.Events.DTSTART, calendar.getTimeInMillis());
        calendar.set(2020, 11, 7, 15, 0);
        contentValues.put(CalendarContract.Events.DTEND, calendar.getTimeInMillis());
        contentValues.put(CalendarContract.Events.CALENDAR_ID, 1);
        contentValues.put(CalendarContract.Events.EVENT_TIMEZONE, "Own home");

        Uri uri = contentResolver.insert(CalendarContract.Events.CONTENT_URI, contentValues);
        long eventId = Long.parseLong(uri.getLastPathSegment());

        ContentValues reminderValues = new ContentValues();
        reminderValues.put(CalendarContract.Reminders.EVENT_ID, eventId);
        reminderValues.put(CalendarContract.Reminders.MINUTES, 15);
        reminderValues.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        contentResolver.insert(CalendarContract.Reminders.CONTENT_URI, reminderValues);
    }
}